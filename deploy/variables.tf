variable "prefix" {
  default = "recipeapp"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable contact {
  default = "taha9107@gmail.com"
}

variable "db_username" {
  description = "Username for the RDS postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"
}